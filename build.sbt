name := """chamelean"""
version := "1.0-SNAPSHOT"
lazy val scalaV = "2.11.6"

fork in run := true

def slf4jVersion = "1.6.3"
def springVersion = "3.0.6.RELEASE"
def jettyVersion = "6.1.25"
lazy val core = (project in file("core")).settings(
  scalaVersion := scalaV,
  libraryDependencies ++= Seq(
    "org.slf4j" % "slf4j-api" % slf4jVersion,
    "org.scala-lang.modules" %% "scala-xml" % "1.0.4",
    "javax.servlet" % "servlet-api" % "2.5",
    "org.springframework" % "spring-core" % springVersion,
    "org.springframework" % "spring-web" % springVersion,
    "commons-io" % "commons-io" % "2.0.1",
    "org.mortbay.jetty" % "jetty" % jettyVersion
  )
)

lazy val themes = (project in file("themes")).settings(
  scalaVersion := scalaV
).dependsOn(core)

lazy val examples = (project in file("examples")).settings(
  scalaVersion := scalaV,
  libraryDependencies ++= Seq(
    "org.slf4j" % "slf4j-log4j12" % slf4jVersion
  )
).dependsOn(core, themes)

// loads the example project at sbt startup
onLoad in Global := (Command.process("project examples", _: State)) compose (onLoad in Global).value
