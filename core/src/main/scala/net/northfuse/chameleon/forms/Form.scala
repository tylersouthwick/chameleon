package net.northfuse.chameleon.forms

import javax.servlet.http.{HttpServletRequest => Request}
import xml.NodeSeq
import collection.JavaConversions._
import net.northfuse.chameleon.Application
import java.util.UUID

/**
 * Follows the POST-REDIRECT-GET pattern so that the user will never get a warning to resubmit a form.
 * @author Tyler Southwick
 */
trait Form[T] {

	import Form.FORM_ACTION_PARAM
	import Application.url

	def actions : Seq[FormAction[T]]
	
	def apply(request : Request) : T
	
	private def formAction() : String = url((request, response) => {
		val parameters = request.getParameterMap.asInstanceOf[java.util.Map[String, Array[String]]]
		val formAction = parameters.entrySet().find(_.getKey.startsWith(FORM_ACTION_PARAM)) match {
			case Some(param) => param.getKey.substring(FORM_ACTION_PARAM.length)
			case None => throw new IllegalStateException("Argument Excepted: " + FORM_ACTION_PARAM)
		}
		actions.find(_.name == formAction) match {
			case Some(callback) => response.sendRedirect(url(callback(apply(request))))
			case None => throw new IllegalStateException("Invalid form action: " + formAction)
		}
	})

	/**
	 * Builds a form with the specified action.
	 * Follows the POST-REDIRECT-GET pattern so that the user will never get a warning to resubmit a form.
	 *
	 * @param body The form body
	 * @return The xhtml of the form
	 */
	final def apply(body: NodeSeq) = {
		<form action={formAction()} method="POST">
			{body}
		</form>
	}
}

object Form {
	val FORM_ACTION_PARAM = "formActionParam_"
}

trait FormAction[T] extends (T => Application.ChameleonCallback) {
	final val name = UUID.randomUUID().toString
	def button(text : String) = <input type="submit" name={Form.FORM_ACTION_PARAM + name} value={text} />
}

